###
create modal very simply.


![alt text](screenshots/01.png)


### Usage
```html
<modal [ngClass]="{show:modal}" (onclose)="modal = false" title="title">content</modal>
```
### Examples

##### *.component.ts
```typescript
@Component(/**/)
export class Component implements OnInit {
  modal:boolean = false;
}
```
##### *.component.html
```html
<modal [ngClass]="{show:modal}" (onclose)="modal = false"></modal>
<button (click)="modal = true">Modal</button>
```
