import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
    @Input() title: string = 'Title';
    @Input() show: boolean;

    @Output() onclose: EventEmitter<any> = new EventEmitter();

    ngOnInit(): void {}
}