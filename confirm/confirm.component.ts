import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'modal-confirm',
    templateUrl: './confirm.component.html',
    styleUrls: ['./confirm.component.css']
})
export class ModalConfirmComponent {
    @Input() title: string = 'Title';
    @Input() show: boolean;

    @Output() onconfirm: EventEmitter<any> = new EventEmitter;
    @Output() onclose: EventEmitter<any> = new EventEmitter;

    confirm() {
        this.onconfirm.emit();
    }
    cancel() {
        this.show = false;
        this.onclose.emit();
    }
}