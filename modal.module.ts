import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ModalComponent } from './modal/modal.component';
import { ModalConfirmComponent } from './confirm/confirm.component';

@NgModule({
  imports: [
    FormsModule,
    BrowserModule,

  ],
  declarations: [
    ModalComponent,
    ModalConfirmComponent,
  ],
  exports: [
    ModalComponent,
    ModalConfirmComponent,
  ]
})
export class ModalModule { }
